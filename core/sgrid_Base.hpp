#ifndef SGRID_BASE_HPP
#define SGRID_BASE_HPP

#include "sgrid_config.hpp"
#include <cassert>

#define SGRID_UNUSED(_macro_x) (void)_macro_x

// #define PDELAB_CPU_ONLY

#endif // SGRID_BASE_HPP